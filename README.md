# highcharts-export-docker 
### (forked from https://github.com/ONSdigital/highcharts-export-docker)

This repo contains what is required to build the docker image for the highcharts export server. It uses the more recent node implementation of the highcharts export server: 

https://github.com/highcharts/node-export-server
